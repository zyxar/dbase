/*
   Ported to Go Lang, by Marcus Zy <chou.marcus@gmail.com>
   10-04-2012
*/
/*
 * These functions are used to read dbase files.
 * *
 * The program is for test purposes only.  No warranty is expressed nor
 * implied. USE AT YOUR OWN RISK!
 *
 *
 */

package dbase

import (
    "fmt"
    "io/ioutil"
)

const (
    DB_FL_BROWSE  = 0x01
    DB_FL_INFO    = 0x02
    DB_FL_DESCR   = 0x04
    DB_FL_RESERVE = 0x08
    DB_FL_OMIT    = 0x10
    DB_FL_TRIM    = 0x20
    DB_FL_DELETED = 0x40
    DB_FLD_CHAR   = 'C'
    DB_FLD_NUM    = 'N'
    DB_FLD_LOGIC  = 'L'
    DB_FLD_MEMO   = 'M'
    DB_FLD_DATE   = 'D'
)

type dBaseHead struct {
    version  uint8   /* 03 for dbIII and 83 for dbIII w/memo file */
    l_update []uint8 /* yymmdd for last update: size 3*/
    count    uint32  /* number of records in file*/
    header   uint16  /* length of the header, includes the \r at end */
    lrecl    uint16  /* length of a record, includes the delete byte */
    reserv   []uint8 /* size 20 */
}

type dBaseField struct {
    name  []uint8 /*field name: length 11*/
    genre uint8   /*field type*/
    /* A-T uses large data model but drop it for now */
    data_ptr  uintptr /*pointer into buffer*/
    length    uint8   /*field length*/
    dec_point uint8   /*field decimal point*/
    fill      []uint8 /* length 14 */
}

type dBaseRecord struct {
    data []byte
}

type dBaseFile struct {
    head   dBaseHead
    base   []byte
    fields []dBaseField
    entry  []dBaseRecord
}

type DbReader dBaseFile
type DbWriter dBaseFile

func NewDBReader(path string) (ret *DbReader, err error) {
    ret = new(DbReader)
    ret.base, err = ioutil.ReadFile(path)
    if err != nil {
        return nil, err
    }
    ret.readHead()
    _, err = ret.verify()
    if err != nil {
        return nil, err
    }
    nf := (ret.head.header-1)/32 - 1
    ret.fields = make([]dBaseField, nf)
    ret.readFields(nf)
    ret.entry = make([]dBaseRecord, ret.head.count)
    return ret, nil
}

func NewDBWriter(path string) (writer *DbWriter, err error) {
    writer = new(DbWriter)
    writer.base, err = ioutil.ReadFile(path)
    if err != nil {
        return nil, err
    }
    writer.readHead()
    _, err = writer.verify()
    if err != nil {
        return nil, err
    }
    nf := (writer.head.header-1)/32 - 1
    writer.fields = make([]dBaseField, nf)
    writer.readFields(nf)
    writer.entry = make([]dBaseRecord, writer.head.count)
    return writer, nil
}

func NewDBVWriter() (writer *DbWriter, err error) {
    writer = new(DbWriter)
    return writer, nil
}

func (w *DbWriter) Flush() {

}

func (w *DbWriter) genHead() {

}

func (w *DbWriter) genFields() {

}

func (w *DbWriter) Insert(rec []byte) error {
    return nil
}

func (w *DbWriter) Append(rec []byte) error {
    return nil
}

func (f *DbReader) RCount() uint32 {
    return f.head.count
}

func (f *DbReader) RLen() uint16 {
    return f.head.lrecl
}

func (f *DbReader) FCount() uint16 {
    return uint16(len(f.fields))
}

func (f *DbReader) FReadLen(p []uint8) {
    for i, v := range f.fields {
        p[i] = v.length
    }
}

func (f *DbReader) DumpFile() {

}

func (f *DbReader) DumpJSON() {

}

func (f *DbReader) verify() (bool, error) {
    if !(f.head.version == 3 || f.head.version == 0x83) {
        if f.head.version == 0x8b {
            err := fmt.Errorf("dBase IV version %d - partially known...", f.head.version)
            return false, err
        }
        err := fmt.Errorf("Unsupported version %d", f.head.version)
        return false, err
    }
    return true, nil
}

func (f *DbWriter) verify() (bool, error) {
    if !(f.head.version == 3 || f.head.version == 0x83) {
        if f.head.version == 0x8b {
            err := fmt.Errorf("dBase IV version %d - partially known...", f.head.version)
            return false, err
        }
        err := fmt.Errorf("Unsupported version %d", f.head.version)
        return false, err
    }
    return true, nil
}

func (f *DbWriter) readHead() {
    f.head.version = read1byte(f.base, 0x00)
    f.head.l_update = f.base[0x01:0x04]
    f.head.count = read4byte(f.base, 0x04)
    f.head.header = read2byte(f.base, 0x08)
    f.head.lrecl = read2byte(f.base, 0x0A)
    f.head.reserv = f.base[0x0C:0x20]
}

func (f *DbWriter) readFields(nf uint16) {
    cur := uint32(0x20)
    for i := uint16(0); i < nf; i++ {
        f.fields[i].name = f.base[cur : cur+11]
        f.fields[i].genre = read1byte(f.base, cur+11)
        f.fields[i].data_ptr = (uintptr)(read4byte(f.base, cur+12))
        f.fields[i].length = read1byte(f.base, cur+16)
        f.fields[i].dec_point = read1byte(f.base, cur+17)
        f.fields[i].fill = f.base[cur+18 : cur+32]
        cur += 0x20
    }
}

func (f *DbReader) readHead() {
    f.head.version = read1byte(f.base, 0x00)
    f.head.l_update = f.base[0x01:0x04]
    f.head.count = read4byte(f.base, 0x04)
    f.head.header = read2byte(f.base, 0x08)
    f.head.lrecl = read2byte(f.base, 0x0A)
    f.head.reserv = f.base[0x0C:0x20]
}

func (f *DbReader) readFields(nf uint16) {
    cur := uint32(0x20)
    for i := uint16(0); i < nf; i++ {
        f.fields[i].name = f.base[cur : cur+11]
        f.fields[i].genre = read1byte(f.base, cur+11)
        f.fields[i].data_ptr = (uintptr)(read4byte(f.base, cur+12))
        f.fields[i].length = read1byte(f.base, cur+16)
        f.fields[i].dec_point = read1byte(f.base, cur+17)
        f.fields[i].fill = f.base[cur+18 : cur+32]
        cur += 0x20
    }
}

func read1byte(base []byte, offset uint32) (ret uint8) {
    ret = base[offset]
    return
}

func read2byte(base []byte, offset uint32) (ret uint16) {
    ret = uint16(base[offset]) | uint16(base[offset+1])<<8
    return
}

func read4byte(base []byte, offset uint32) (ret uint32) {
    ret = uint32(base[offset]) | uint32(base[offset+1])<<8 | uint32(base[offset+2])<<16 | uint32(base[offset+3])<<24
    return
}

func trim(src []byte) []byte {
    h, t := 0, len(src)
    for src[h] == ' ' {
        h++
    }
    for src[t-1] == ' ' {
        t--
    }
    return src[h:t]
}

func trimLeft(src []byte) []byte {
    h := 0
    for src[h] == ' ' {
        h++
    }
    return src[h:]
}

func trimRight(src []byte) []byte {
    t := len(src)
    for src[t-1] == ' ' {
        t--
    }
    return src[:t]
}

func (f *DbReader) Info() {
    fmt.Printf("File version    : %d\n", f.head.version)
    fmt.Printf("Last update     : %02d/%02d/%2d\n", f.head.l_update[1], f.head.l_update[2], uint16(f.head.l_update[0])+1900)
    fmt.Printf("Number of recs  : %d\n", f.head.count)
    fmt.Printf("Header length   : %d\n", f.head.header)
    fmt.Printf("Record length   : %d\n", f.head.lrecl)

    fmt.Printf("\nField Name\tType\tLength\tDecimal Pos\n")
    for _, v := range f.fields {
        fmt.Printf("%-16s\t  %c\t  %3d\t  %3d\n", v.name, v.genre, v.length, v.dec_point)
    }
}

func (f *DbReader) String() (s string) {
    s = fmt.Sprintf("File version    : %d\n", f.head.version) +
        fmt.Sprintf("Last update     : %02d/%02d/%2d\n", f.head.l_update[1], f.head.l_update[2], uint16(f.head.l_update[0])+1900) +
        fmt.Sprintf("Number of recs  : %d\n", f.head.count) +
        fmt.Sprintf("Header length   : %d\n", f.head.header) +
        fmt.Sprintf("Record length   : %d\n", f.head.lrecl) +
        fmt.Sprintf("\nField Name\tType\tLength\tDecimal Pos\n")
    for _, v := range f.fields {
        s += fmt.Sprintf("%-16s\t  %c\t  %3d\t  %3d\n", v.name, v.genre, v.length, v.dec_point)
    }
    return
}

func (f *DbReader) Print(flags int, delim byte, deleted byte) {
    var l uint8
    var nf = len(f.fields)
    var buffer []byte
    for _, v := range f.entry {
        buffer = v.data
        for i := 0; i < nf; i++ {
            l = f.fields[i].length
            fmt.Printf("%-10s\t:%s\n", f.fields[i].name, buffer[:l])
            buffer = buffer[l:]
        }
        fmt.Printf("\n")
    }
}

func (f *DbReader) Read(index uint32) ([]byte, error) {
    if uint32(len(f.entry)) <= index {
        err := fmt.Errorf("index out of range\n")
        return nil, err
    }
    return f.entry[index].data, nil
}

func (f *DbReader) Process() {
    offset := f.head.header
    rlen := f.head.lrecl
    data := f.base[offset:]
    for cnt := f.head.count; cnt > 0; cnt-- {
        f.entry[f.head.count-cnt].data = data[0:rlen]
        data = data[rlen:]
    }
}
